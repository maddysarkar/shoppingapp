﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingAppLib
{
    public interface IOrderService<T>
    {
        /// <summary>
        /// Create or update Order of type T
        /// </summary>
        /// <param name="orderDetails"></param>
        /// <returns></returns>
        Task<double> CreateOrUpdateOrder(T orderDetails);

        /// <summary>
        /// Get Order details of type T using orderId
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        T GetOrderDetails(int orderId);

        /// <summary>
        /// Get List of Product Name 
        /// </summary>
        /// <returns></returns>
        List<string> GetProductNameList();
    }
}
