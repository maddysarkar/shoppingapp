﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ShoppingAppLib
{
    public interface IUtility<T>
    {
       bool ValidateList(List<T> details);
       double GetRequiredBinWidth(List<T> details);
    }
}
