﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ShoppingAppLib
{
    public class ShoppingDBDataContext: DbContext
    {

        public DbSet<Product> Product { get; set; }
        public DbSet<Order> Order { get; set; }
        public ShoppingDBDataContext(DbContextOptions options) : base(options)
        {
        }
    }
}
