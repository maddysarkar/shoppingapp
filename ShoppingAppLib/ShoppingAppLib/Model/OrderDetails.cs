﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace ShoppingAppLib
{
    public class OrderDetails
    {
        [Required]
        public int OrderId { get; set; }
        [Required]
        public List<ProductDetails> ProductList { get; set; }
        
        public double OrderBinWidthInMM { get; set; }

    }
}
