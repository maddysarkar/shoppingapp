﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingAppLib
{
    public class Order
    {
        public int Id { get; set; }

        public string Details { get; set; }

        public double RequiredBinWidthInMM { get; set; }
    }
}
