﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShoppingAppLib
{
    public class ProductDetails
    {
        [Required]
        public string ProductName { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}
