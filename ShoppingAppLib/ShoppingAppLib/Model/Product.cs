﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingAppLib
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double WidthInMM { get; set; }
        public int MaxCountInOneStack { get; set; }

    }
}
