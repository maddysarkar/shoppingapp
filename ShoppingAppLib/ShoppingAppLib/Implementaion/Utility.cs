﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace ShoppingAppLib
{
    public class Utility : IUtility<ProductDetails>
    {
        ShoppingDBDataContext _dbContext;
      

        public Utility(ShoppingDBDataContext dbContext)
        {
            _dbContext = dbContext;
        }

     

        public double GetRequiredBinWidth(List<ProductDetails> details)
        {
            try
            {             

                // To calculate Bin Width we will consider MaxCountPerBinAsOne for each product 
                var productInfoListForBinSize = (from orderedProduct in details
                                                 join product in _dbContext.Product on orderedProduct.ProductName equals product.Name
                                                 select new 
                                                 {
                                                     Name = orderedProduct.ProductName,
                                                     Quantity = System.Convert.ToInt32(Math.Ceiling((double)orderedProduct.Quantity / product.MaxCountInOneStack)),
                                                     WidthInMM = product.WidthInMM
                                                 }).ToList();

                double requiredBinWidth = productInfoListForBinSize.Select(x => x.Quantity * x.WidthInMM).Sum();

                return requiredBinWidth;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ValidateList(List<ProductDetails> details)
        {
            try
            {        
                
                var orderedProduct = details.Select(x => x.ProductName).Distinct().ToList();
                var productsAvailable = _dbContext.Product.Select(p => p.Name.ToLower()).Distinct().ToList();
                bool productListValid = !orderedProduct.Except(productsAvailable).Any();
                return productListValid;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
