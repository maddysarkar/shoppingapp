﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace ShoppingAppLib
{
    public class OrderService : IOrderService<OrderDetails>
    {
        private ShoppingDBDataContext _dbContext;
        public IUtility<ProductDetails> _utility;
        public OrderService(ShoppingDBDataContext dbContext, IUtility<ProductDetails> utility)
        {
            _dbContext = dbContext;
            _utility = utility;
        }

        public async Task<double> CreateOrUpdateOrder(OrderDetails orderDetails)
        {
            try
            {
                if (orderDetails == null || orderDetails.OrderId <= 0 || orderDetails.ProductList == null || orderDetails.ProductList.Count == 0)
                    throw new ArgumentException("Invalid input");

                // In case of duplicate product entry consider them as one and add quantities
                var finalProductList = orderDetails.ProductList.GroupBy(x => x.ProductName)
                                           .Select(cl => new ProductDetails
                                           {
                                               ProductName = cl.Select(x => x.ProductName).FirstOrDefault(),
                                               Quantity = cl.Sum(c => c.Quantity),
                                           }).ToList();

                if (_utility.ValidateList(finalProductList) == false)
                {
                    throw new ArgumentException("Product list is not valid");
                }
                var order = _dbContext.Order.Where(o => o.Id == orderDetails.OrderId).FirstOrDefault();
                if (order != null)
                {
                    _dbContext.Order.Remove(order);
                }
                order = new Order();
                order.Id = orderDetails.OrderId;
                order.Details = JsonConvert.SerializeObject(finalProductList);
                order.RequiredBinWidthInMM = _utility.GetRequiredBinWidth(finalProductList);
                _dbContext.Order.Add(order);
                await _dbContext.SaveChangesAsync();
                return order.RequiredBinWidthInMM;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public OrderDetails GetOrderDetails(int orderId)
        {
            try
            {
                if (orderId <= 0)
                    throw new ArgumentException("Invalid input");
                OrderDetails orderDetails = new OrderDetails();
                var order =  _dbContext.Order.Where(o => o.Id == orderId).FirstOrDefault();
                if (order != null && !string.IsNullOrEmpty(order.Details))
                {
                    orderDetails.OrderId = order.Id;
                    orderDetails.ProductList = JsonConvert.DeserializeObject<List<ProductDetails>>(order.Details);
                    orderDetails.OrderBinWidthInMM = order.RequiredBinWidthInMM;
                    return orderDetails;
                }
                
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetProductNameList()
        {
            try
            {
                var productNameList = _dbContext.Product.Select(p => p.Name).Distinct().ToList();
                return productNameList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
