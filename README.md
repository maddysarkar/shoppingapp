# ShoppingApp:

***
##
	ShoppingApp allows customer to order 1 or multiple items from list of available Products in DB.
	After the order is produced, it is delivered to one out of thousands of pickup points across the country.
	The package is put in a bin on a shelf at the pickup point. The bin has to be sufficiently wide for the
	package. Since bins are reserved upfront, so ShoppingApp calculates max bin width for packing Order. 
	
***

## Below are the projects in ShoppingApp solution

## ShoppingAppLib 

- Contains business logic for CreateOrUpdateOrder, GetOrderDetails and GetProductNameList.
- CreateOrUpdateOrder is used to create new order or to update exisitng order.
- It has Utility methods ValidateList and GetRequiredBinWidth.
- ValidateList is used to check entered Product list in Order are valid or not.
- GetRequiredBinWidth is used to get max bin width for packing Order.
- GetProductNameList is used to get Product name list available.

## ShoppingAppAPI

- Consumes ShoppingAppLib to to process API requests and response to User.

*** ShoppingAppAPI Exposes three APIs as follows: ***

##

			[HttpPost]
			/CreateOrUpdateOrder

			 Payload:

			 {
			"orderId": 0,
			"productList": [
			{
			  "productName": "string",
			  "quantity": 0
			}
			],
			 "orderBinWidthInMM": 0
			}

			[HttpGet]
			/GetOrderDetails?orderId=1
			
			[HttpGet]
			/GetProductNameList

    
## ShoppingAppAPITest

- Contains possible tests covering all scenarios related to Creating/Updating and getting Order details. Also test for GetProductNameList.

*** 

### Steps to SetUp DB: 

- Run below scipts to make db ready:

##
		CREATE TABLE [dbo].[Product]
		(
			[Id] INT NOT NULL PRIMARY KEY, 
			[Name] VARCHAR(50) NOT NULL, 
			[WidthInMM] FLOAT NOT NULL, 
			[MaxCountInOneStack] INT NOT NULL
		)

		INSERT INTO [dbo].[Product] ([Id], [Name], [WidthInMM], [MaxCountInOneStack]) VALUES (1, N'photobook', 19 ,1)
		INSERT INTO [dbo].[Product] ([Id], [Name], [WidthInMM], [MaxCountInOneStack]) VALUES (4, N'calendar', 10,1)
		INSERT INTO [dbo].[Product] ([Id], [Name], [WidthInMM], [MaxCountInOneStack]) VALUES (2, N'canvas', 16,1)
		INSERT INTO [dbo].[Product] ([Id], [Name], [WidthInMM], [MaxCountInOneStack]) VALUES (3, N'cards', 4.7,1)
		INSERT INTO [dbo].[Product] ([Id], [Name], [WidthInMM], [MaxCountInOneStack]) VALUES (5, N'mug', 94, 4)


		CREATE TABLE [dbo].[Order]
		(
			[Id] INT NOT NULL PRIMARY KEY, 
			[Details] VARCHAR(MAX) NOT NULL, 
			[RequiredBinWidthInMM] FLOAT NOT NULL,

		)

- Replace "Storage" in "ShoppingAppAPI/appsettings.Development.json" with corresponding connectionstring where DB is located.





