using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingAppAPI.Controllers;
using ShoppingAppLib;
using Xunit;

namespace ShoppingAppAPITest
{
    public class ShoppingAppAPITest
    {
        [Fact]
        public async void TestInValidProductListInput()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestInValidProductListInput")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);
                OrderDetails newOrder = new OrderDetails
                {
                    OrderId = 1
                };

                var result = await orderServiceController.CreateOrUpdateOrder(newOrder);
                var resultObject = result as ObjectResult;
                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status422UnprocessableEntity);
            }
        }


        [Fact]
        public async void TestOrderProductNotInDB()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestOrderProductNotInDB")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);

                OrderDetails newOrder = new OrderDetails
                {
                    OrderId = 1,
                    ProductList = new List<ProductDetails>
                    {
                        new ProductDetails{ProductName = "book",Quantity=2},
                        new ProductDetails{ProductName="mug",Quantity=5}
                    }
                };

                var result = await orderServiceController.CreateOrUpdateOrder(newOrder);
                var resultObject = result as ObjectResult;
                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status422UnprocessableEntity);
            }
        }

        [Fact]
        public async void TestNegativeOrderId()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestNegativeOrderId")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);
                OrderDetails newOrder = new OrderDetails
                {
                    OrderId = -1,
                    ProductList = new List<ProductDetails>
                    {
                        new ProductDetails{ProductName = "photobook",Quantity=2},
                        new ProductDetails{ProductName="mug",Quantity=5}
                    }
                };
                var result = await orderServiceController.CreateOrUpdateOrder(newOrder);
                var resultObject = result as ObjectResult;
                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status422UnprocessableEntity);
            }

        }
        [Fact]
        public async void TestValidOrder()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestValidOrder")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);
                OrderDetails newOrder = new OrderDetails
                {
                    OrderId = 1,
                    ProductList = new List<ProductDetails>
                    {
                        new ProductDetails{ProductName = "photobook",Quantity=2},
                        new ProductDetails{ProductName="mug",Quantity=5}
                    }
                };

                var result = await orderServiceController.CreateOrUpdateOrder(newOrder);
                var resultObject = result as ObjectResult;
                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status201Created);
            }
        }

       

        [Fact]
        public void TestGetInvalidOrder()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestGetInvalidOrder")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);
                OrderDetails newOrder = new OrderDetails
                {
                    OrderId = 1,
                    ProductList = new List<ProductDetails>
                    {
                        new ProductDetails{ProductName = "cards",Quantity=2},
                        new ProductDetails{ProductName="mug",Quantity=5},
                        new ProductDetails{ProductName="mug",Quantity=5}
                    }
                };

                var result = orderServiceController.GetOrderDetails(-1);
                var resultObject = result as ObjectResult;
                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status422UnprocessableEntity);
            }
        }

        [Fact]
        public void TestGetNonExistingOrder()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestGetNonExistingOrder")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {
                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);

                var result = orderServiceController.GetOrderDetails(5);
                var resultObject = result as ObjectResult;
                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status204NoContent);
            }
        }

        [Fact]
        public async void TestGetValidExistingOrder()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestGetValidExistingOrder")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);
                OrderDetails newOrder = new OrderDetails
                {
                    OrderId = 5,
                    ProductList = new List<ProductDetails>
                    {
                        new ProductDetails{ProductName = "cards",Quantity=2},
                        new ProductDetails{ProductName="mug",Quantity=5},
                        new ProductDetails{ProductName="mug",Quantity=5}
                    }
                };
                //Insert valid order for retrieval
                var addOrder = await orderServiceController.CreateOrUpdateOrder(newOrder);
                var result = orderServiceController.GetOrderDetails(5);
                var resultObject = result as ObjectResult;

                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status200OK);

                OrderDetails orderResult = (OrderDetails)resultObject.Value;

                //As per MaxCountInOneStack for mug says 4 mugs can be stacked in one, 
                // so in this order 10 mugs are quivalent to 3 mugs space so 3 * 94 = 282
                // and 2 photobook equals 2 * 4.7 = 9.4, so RequiredBinWidthInMM = 291.4
                Assert.True(orderResult.OrderBinWidthInMM == 291.4);

                //Assert on types of products in Order are two
                Assert.True(orderResult.ProductList.Count == 2);
            }

        }

        [Fact]
        public void TestGetProductNameListEmpty()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestGetProductNameListEmpty")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);

                var productNameList = orderServiceController.GetProductNameList();
                var resultObject = productNameList as ObjectResult;

                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status204NoContent);
            }
        }

        [Fact]
        public void TestGetProductNameList()
        {
            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseInMemoryDatabase(databaseName: "TestGetProductNameList")
                       .Options;

            using (var dbContext = new ShoppingDBDataContext(options))
            {

                dbContext.Product.AddRange(GetProducts());
                dbContext.SaveChanges();
                IOrderService<OrderDetails> orderService = new OrderService(dbContext, new Utility(dbContext));
                OrderServiceController orderServiceController = new OrderServiceController(orderService);
                var productNameList = orderServiceController.GetProductNameList();
                var resultObject = productNameList as ObjectResult;

                Assert.True(resultObject.StatusCode == Microsoft.AspNetCore.Http.StatusCodes.Status200OK);
            }

        }
        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>
            {
            new Product{Id=1,Name="photobook",WidthInMM=19,MaxCountInOneStack=1 },
            new Product{Id=2,Name="canvas",WidthInMM=16,MaxCountInOneStack=1 },
            new Product{Id=3,Name="cards",WidthInMM=4.7,MaxCountInOneStack=1 },
            new Product{Id=4,Name="calendar",WidthInMM=10,MaxCountInOneStack=1 },
            new Product{Id=5,Name="mug",WidthInMM=94,MaxCountInOneStack=4 },
            };
            return products;

        }
    }
}


