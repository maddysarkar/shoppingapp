﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ShoppingAppLib;

namespace ShoppingAppAPI.Controllers
{
    public class OrderServiceController : Controller
    {
        private IOrderService<OrderDetails> _orderService;

        public OrderServiceController(IOrderService<OrderDetails> orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        [Route("CreateOrUpdateOrder")]
        public async Task<IActionResult> CreateOrUpdateOrder([FromBody]OrderDetails orderDetails)
        {
            try
            {
                if (!ModelState.IsValid)
                    return StatusCode(StatusCodes.Status400BadRequest, "Model is not valid");
                var result = await _orderService.CreateOrUpdateOrder(orderDetails);
                return StatusCode(StatusCodes.Status201Created, "The minimum bin width: " + result + " mm");
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status422UnprocessableEntity, ex.Message.ToString());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message.ToString());
            }
        }

        [HttpGet]
        [Route("GetOrderDetails")]
        public IActionResult GetOrderDetails(int orderId)
        {
            try
            {
                if (!ModelState.IsValid)
                    return StatusCode(StatusCodes.Status400BadRequest, "Model is not valid");
                var result = _orderService.GetOrderDetails(orderId);

                if (result != null)
                {

                    return StatusCode(StatusCodes.Status200OK, result);
                }
                else
                {
                    return StatusCode(StatusCodes.Status204NoContent, "No records found");
                }
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status422UnprocessableEntity, ex.Message.ToString());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message.ToString());
            }
        }


        [HttpGet]
        [Route("GetProductNameList")]
        public IActionResult GetProductNameList()
        {
            try
            {

                var result = _orderService.GetProductNameList();

                if (result != null && result.Count > 0)
                {

                    return StatusCode(StatusCodes.Status200OK, result);
                }
                else
                {
                    return StatusCode(StatusCodes.Status204NoContent, "No records found");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message.ToString());
            }
        }
    }
}