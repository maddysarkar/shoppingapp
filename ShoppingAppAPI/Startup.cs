﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShoppingAppLib;
using Swashbuckle.AspNetCore.Swagger;

namespace ShoppingAppAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            string connString = Configuration.GetSection("MySettings").GetSection("Storage").Value;

            var options = new DbContextOptionsBuilder<ShoppingDBDataContext>()
                       .UseSqlServer(connString)
                       .Options;

            ShoppingDBDataContext dbContext = new ShoppingDBDataContext(options);

            services.AddSingleton(_ => new OrderService(dbContext, new Utility(dbContext)) as IOrderService<OrderDetails>);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Dynamic configuration structure APIs" });
            });



            // Inject an implementation of ISwaggerProvider with defaulted settings applied
            services.AddSwaggerGen();

            //Added CORS suppo
            services.AddCors(o => o.AddPolicy("AllowSpecificOrigin", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowSpecificOrigin");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                });
            }
            app.UseMvc();
        }
    }
}
